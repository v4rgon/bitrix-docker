
## Введение
 Запускает битрикс предоставляя готовые сервисы PHP, Tengine, MySQL и многие другие.


## Автоматическая установка
```
curl -fsSL 'https://gitlab.com/v4rgon/bitrix-docker/-/raw/master/install.sh' -o install.sh && chmod +x install.sh && sh install.sh
```

### Выполните настройку окружения

Скопируйте файл `.env_template` в `.env`

```
cp -f .env_template .env
```


По умолчанию используется tengine, php7, mysql. Настройки можно изменить в файле ```.env```. Также можно задать путь к каталогу с сайтом и параметры базы данных MySQL.


```
PHP_VERSION=php74          # Версия php
WEB_SERVER_TYPE=nginx      # Веб-сервер nginx/apache
DB_SERVER_TYPE=mysql       # Сервер базы данных mysql/percona
MYSQL_DATABASE=bitrix      # Имя базы данных
MYSQL_USER=bitrix          # Пользователь базы данных
MYSQL_PASSWORD=123         # Пароль для доступа к базе данных
MYSQL_ROOT_PASSWORD=123    # Пароль для пользователя root от базы данных
INTERFACE=0.0.0.0          # На данный интерфейс будут проксироваться порты
SITE_PATH=/var/www/bitrix  # Путь к директории Вашего сайта

```
</p>
</details>

## Запуск и остановка 
### Запуск
```
docker-compose up -d
```
Чтобы проверить, что все сервисы запустились посмотрите список процессов ```docker ps```.
Посмотрите все прослушиваемые порты, должны быть 80, 11211, 9000 ```netstat -plnt```.
Откройте IP машины в браузере.

### Остановка
```
docker-compose down
```
## Как заполнять подключение к БД
![db](https://gitlab.com/v4rgon/bitrix-docker/-/raw/master/db.png)

## Примечание
- По умолчанию стоит папка ```/var/www/bitrix/```
- В настройках подключения требуется указывать имя сервиса, например для подключения к базе нужно указывать "db", а не "localhost". Пример [конфига](configs/.settings.php) с подключением к mysql и memcached.
- Для загрузки резервной копии в контейнер используйте команду: ```cat /var/www/bitrix/backup.sql | docker exec -i mysql /usr/bin/mysql -u root -p123 bitrix```
- При использовании php74 в production удалите строку с `php7.4-xdebug` из файла `php74/Dockerfile`, сам факт его установки снижает производительность Битрикса и он должен использоваться только для разработки

## Использование xdebug.

- Настройки xdebug задаются в `phpXX/php.ini`.
- Для php73, php74 дефолтовые настройки xdebug - коннект на порт `9003` хоста, с которого пришел запрос. В случае невозможности коннекта - фаллбек на `host.docker.internal`.
- При изменении `php.ini` в проекте не забудьте добавить флаг `--build` при запуске `docker-compose`, чтобы форсировать пересборку имиджа.






